import { request, requestDefault } from '@/utils/request.js'

/*
	获取文章列表 (视频接口已失效)
 */
export const getArticles = (queryInfo) => {
	return request({
		method: 'GET',
		url: 'app/v1_0/articles',
		// params用来设置Get的参数
		params: queryInfo
	})
}

/*
	获取文章列表2 (本地测试数据)
 */
export const getArticlesLocal = (queryInfo) => {
	return requestDefault({
		method: 'GET',
		url: '/articles.json',
		// params用来设置Get的参数
		params: queryInfo
	})
}

/*
	获取文章频道
 */
export const getChannels = () => {
	return request({
		method: 'GET',
		url: 'app/v1_0/channels'
	})
}

/*
	删除指定文章
 */
export const delteArticle = (id) => {
	return request({
		method: 'DELETE',
		url: `app/v1_0/articles/${id}`
	})
}

/*
	发布文章
 */
export const publishArticle = (data, draft = false) => {
	return request({
		method: 'POST',
		url: '/app/v1_0/articles',
		params: {
			draft // 是否存为草稿 （true 为存草稿）
		},
		data
	})
}

/*
	获取指定文章
 */
export const getArticleById = (articleId) => {
	return request({
		method: 'GET',
		url: `/app/v1_0/articles/${articleId}`
	})
}

/*
	修改指定文章
 */
export const updateArticleById = (articleId, data, draft = false) => {
	return request({
		method: 'PUT',
		url: `/app/v1_0/articles/${articleId}`,
		params: {
			draft
		},
		data
	})
}

/*
	获取文章评论状态 (本地测试数据)
 */
export const getCommentLocal = (queryInfo) => {
	return requestDefault({
		method: 'GET',
		url: '/comment.json',
		// params用来设置Get的参数
		params: queryInfo
	})
}

/*
	修改文章评论状态
 */
export const updateCommentStatus = (articleId, status) => {
	return requestDefault({
		method: 'PUT',
		url: '/app/v1_0/comments/status',
		// params用来设置Get的参数
		params: {
			article_id: articleId
		},
		data: {
			allow_comment: status
		}
	})
}

/*
	上传素材
 */
export const updateImage = (data) => {
	return request({
		method: 'POST',
		url: '/app/v1_0/user/images',
		data
	})
}
