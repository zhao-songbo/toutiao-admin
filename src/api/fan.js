import { request, requestDefault } from '@/utils/request.js'

/*
 *
 * 获取粉丝列表 (数据接口已失效)
 *
 */

export const getFansList = (params) => {
	return request({
		method: 'GET',
		url: '/app/v1_0/followers',
		params
	})
}

/*
 *
 * 获取粉丝列表 (本地模拟数据)
 *
 */

export const getFansListLocal = (params) => {
	return requestDefault({
		method: 'GET',
		url: '/fans.json',
		params
	})
}
