import { request, requestDefault } from '@/utils/request.js'

/*
 *
 * 获取图片素材 （接口已失效）
 *
 */

export const getImagesSource = (query) => {
	return request({
		method: 'GET',
		url: '/app/v1_0/user/images',
		params: query
	})
}

/*
 *
 * 获取图片素材 （本地模拟数据）
 *
 */

export const getImitateImagesSource = (query) => {
	return requestDefault({
		method: 'GET',
		url: '/imagesSource.json',
		params: query
	})
}

/*
 *
 * 收藏图片素材
 *
 */

export const updateCollect = (imageId, collect) => {
	return request({
		method: 'PUT',
		url: `/app/v1_0/user/images/${imageId}`,
		data: {
			collect
		}
	})
}

/*
 *
 * 删除图片素材
 *
 */

export const deleteImage = (imageId) => {
	return request({
		method: 'DELETE',
		url: `/app/v1_0/user/images/${imageId}`
	})
}
