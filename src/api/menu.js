import { requestDefault } from '@/utils/request.js'

export const getMenuList = () => {
	return requestDefault({
		method: 'GET',
		url: '/menuList.json'
	})
}
