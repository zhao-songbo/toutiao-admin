import { request } from '@/utils/request.js'

// 用户登录
export const login = (loginInfo) => {
	return request({
		method: 'POST',
		url: 'app/v1_0/authorizations',
		// data用来设置POST的请求体
		data: loginInfo
	})
}

// 获取用户信息
export const getUserInfo = () => {
	return request({
		method: 'GET',
		url: 'app/v1_0/user/profile'
	})
}

// 修改用户信息
export const updateUserInfo = (data) => {
	return request({
		method: 'PATCH',
		url: 'app/v1_0/user/profile',
		data
	})
}

// 修改用户头像
// 注意 data 必须传递 FormData 对象
export const updateUserImage = (data) => {
	return request({
		method: 'PATCH',
		url: 'app/v1_0/user/photo',
		data
	})
}
