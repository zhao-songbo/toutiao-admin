import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 加载 ElementUI 组件库
import ElementUI from 'element-ui'
// 加载 ElementUI 组件库的样式
import 'element-ui/lib/theme-chalk/index.css'
import './style/reset.css'
import './fonts/iconfont.css'
import './style/index.less'

// 全局注册 ElementUI 组件库
Vue.use(ElementUI)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
