import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// @ (src目录别名)

const routes = [
	{
		path: '/',
		redirect: '/login'
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('@/views/login/')
	},
	{
		path: '/home',
		component: () => import('@/views/home/'),
		children: [
			{
				path: '',
				name: 'index',
				component: () => import('@/views/welcome/')
			},
			{
				path: '/article',
				name: 'article',
				component: () => import('@/views/article/')
			},
			{
				path: '/publish',
				name: 'publish',
				component: () => import('@/views/publish/')
			},
			{
				path: '/image',
				name: 'image',
				component: () => import('@/views/image/')
			},
			{
				path: '/comment',
				name: 'comment',
				component: () => import('@/views/comment/')
			},
			{
				path: '/setting',
				name: 'setting',
				component: () => import('@/views/setting/')
			},
			{
				path: '/fans',
				name: 'fans',
				component: () => import('@/views/fans/')
			}
		]
	}
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
	// to => 将要访问的路径
	// from => 从哪个路径跳转而来
	// next => 是个函数，表示放行
	// next()=>放行   next('/login')=>	强制跳转

	if (to.path === '/login') {
		return next()
	}

	// 获取token
	const tokenStr = window.sessionStorage.getItem('token')
	if (!tokenStr) {
		return next('/login')
	} else {
		next()
	}
})

export default router
