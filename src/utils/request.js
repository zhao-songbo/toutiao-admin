/*
 * 基于 axios 封装的请求模块
 */

import axios from 'axios'
import JSONBig from 'json-bigint'

/*
 *
 * 创建一个 axios 实例，说白了就是复制一个 axios
 * 我们通过这个实例去发送请求,把需要的配置给这个实例来处理
 *
 */
const request = axios.create({
	// 请求基础路径
	baseURL: 'http://api-toutiao-web.itheima.net/',

	// 定义后端返回的原始数据处理
	transformResponse: [function (data) { // 参数的 data 是未经过 axios 处理的后端返回原始数据  (未经处理的JSON字符串)
		/*
		 *
		 * 后端返回的数据可能不是 JSON 格式的字符串
		 * 如果不是的话, 那么 JSONBig.parse 调用就会报错
		 * 所以用 try-catch 来补货异常
		 *
		 */
		try {
			// 成功走这里
			// 用 JSONBig 处理后端返回的原始数据
			return 	JSONBig.parse(data)
		} catch (err) {
			// 失败走这里
			// 返回 axios 处理的数据
			return data
		}
	}]
})

// 请求拦截器添加token,保证拥有获取数据的权限
request.interceptors.request.use(config => {
	// 给请求头添加Authorization属性
	config.headers.Authorization = 'Bearer ' + window.sessionStorage.getItem('token')
	// 在最后必须return config （固定写法）
		return config
})

/*
 *
 *
 *
 * 调用本地模拟数据的 axios
 *
 *
 *
 */
const requestDefault = axios.create({
	// 定义后端返回的原始数据处理
	transformResponse: [function (data) { // 参数的 data 是未经过 axios 处理的后端返回原始数据  (未经处理的JSON字符串)
		/*
		 *
		 * 后端返回的数据可能不是 JSON 格式的字符串
		 * 如果不是的话, 那么 JSONBig.parse 调用就会报错
		 * 所以用 try-catch 来补货异常
		 *
		 */
		try {
			// 成功走这里
			// 用 JSONBig 处理后端返回的原始数据
			return 	JSONBig.parse(data)
		} catch (err) {
			// 失败走这里
			// 返回 axios 处理的数据
			return data
		}
	}]
})

export { request, requestDefault }
